
## nsstools

This is a R version of nsstools, handling the non single star (NSS) products provided in [Gaia DR3](https://www.cosmos.esa.int/web/gaia/data-release-3). It allows to:

* for all kind of nss_solution_type, convert the correlation matrix and the uncertainties to the covariance matrix of the solution
* for a NSS astrometric orbital solution, either Orbital* or AstroSpectroSB1, convert the Thiele-Innes orbital elements to the Campbell elements and propagates the uncertainties.
Ref: Halbwachs et al., 2022, Gaia Data Release 3. Astrometric binary star processing, Astronomy and Astrophysics, Appendix A and B. 

A python version is available [here](https://gitlab.obspm.fr/gaia/nsstools).

## Installation

* install Rcpp:

R -e 'install.packages("Rcpp")'

* compile the package:

R CMD INSTALL --preclean --no-multiarch --with-keep.source .

* in R:

library(nsstools)
    
The directory testdata/ contains a sample of each NSS solution type and bit_index. Note that the source_ids and positions have been changed. 

## Usage 

See the [notebook](https://gricad-gitlab.univ-grenoble-alpes.fr/babusiac/nsstools/-/blob/main/nss.ipynb).

## Authors and acknowledgment

Authors: Carine Babusiaux from a code by Jean-Louis Halbwachs.

Reference: [Halbwachs et al., 2022, Gaia Data Release 3. Astrometric binary star processing, Astronomy and Astrophysics, Appendix A and B.](https://ui.adsabs.harvard.edu/abs/2022arXiv220605726H/abstract)



