#include <Rcpp.h>
using namespace Rcpp;
#include <cmath>

// [[Rcpp::export]]
NumericMatrix getCovMat(NumericVector err, NumericVector cor) {
  NumericMatrix covmat = NumericMatrix(err.size(),err.size());
  if (cor.size() != (err.size() * (err.size() - 1) / 2)) {
    stop("vector sizes not compatible");
  }
  int k=0;
  for(int j=0; j<err.size(); j++) { // col
    covmat(j,j) = err[j]*err[j];
    for (int i=0; i<j; i++) { // row
      covmat(i,j) = cor[k]*err[i]*err[j];
      covmat(j,i) = covmat(i,j);
      k++;
    }
  }
  return covmat;
}

// From Jean-Louis Halbwachs Fortran code 
// [[Rcpp::export]]
DataFrame getTICHerrors(NumericMatrix covMat, double A, double B, double F, double G, double C, double H) {
  
  double a0, incl, w, Ome, a1; 
  double siga0, sigincl, sigw, sigOme, siga1; 
  
  double wpOme = atan2( B - F , A + G );  // calcul de omega + Omega
  double wmOme = atan2(-B - F , A - G ); // calcul de omega - Omega
  w     = (wpOme + wmOme) / 2.;  // Premieres estimations entre -pi et +pi
  Ome   = (wpOme - wmOme) / 2.;
  
  if(Ome < 0) {      // Omega doit etre entre 0 et pi; on ajoute donc 2 pi
    Ome = Ome + M_PI; // a wpOme, ou on en retranche autant a wmOme.
    w   = w + M_PI;   // Correction de w en consequence.
  }
        
  // needed ? 
  //Ome = min( Ome, 3.14159265) // Correction d'arrondi pour eviter tout debordement
  //w   = min( max( 0.d00, w) , 6.2831853)

  double ApG2pBmF2 = pow(A+G,2.0) + pow(B-F,2.0);
  double GmA2pBpF2 = pow(G-A,2.0) + pow(B+F,2.0);
  
  double tAw = (F-B)/ApG2pBmF2 + (B+F)/GmA2pBpF2;  // Coef. devant dA
  double tBw = (A+G)/ApG2pBmF2 + (G-A)/GmA2pBpF2;  // Coef. devant dB
  double tFw = -(A+G)/ApG2pBmF2 + (G-A)/GmA2pBpF2; // Coef. devant dF
  double tGw = (F-B)/ApG2pBmF2 - (B+F)/GmA2pBpF2;  // Coef. devant dG
  
  sigw = sqrt( tAw*tAw*covMat(0,0) + tBw*tBw*covMat(1,1) + tFw*tFw*covMat(2,2) +  tGw*tGw*covMat(3,3) 
                 + 2.0 * ( tAw*tBw*covMat(0,1) + tAw*tFw*covMat(0,2) + tBw*tFw*covMat(1,2) + 
                   tAw*tGw*covMat(0,3) + tBw*tGw*covMat(1,3) + tFw*tGw*covMat(2,3) ) ) / 2.0;
  
  double tAO = (F-B)/ApG2pBmF2 - (B+F)/GmA2pBpF2; // Coef. devant dA
  double tBO = (A+G)/ApG2pBmF2 - (G-A)/GmA2pBpF2; // Coef. devant dB
  double tFO = -(A+G)/ApG2pBmF2 - (G-A)/GmA2pBpF2; // Coef. devant dF
  double tGO = (F-B)/ApG2pBmF2 + (B+F)/GmA2pBpF2; // Coef. devant dG
    
  sigOme = sqrt( tAO*tAO*covMat(0,0) + tBO*tBO*covMat(1,1) + tFO*tFO*covMat(2,2) +   tGO*tGO*covMat(3,3) 
                 + 2.0 * ( tAO*tBO*covMat(0,1) +  tAO*tFO*covMat(0,2) +  tAO*tGO*covMat(0,3) +  
                           tBO*tFO*covMat(1,2) +  tBO*tGO*covMat(1,3) +  tFO*tGO*covMat(2,3)) ) / 2.0;
      
  double tg2iAG = std::abs((A + G) * cos(wmOme));  // Calcul des denominateurs des deux
  double tg2iBF = std::abs((F - B) * sin(wmOme));  // formules donnant tan^2 i/2
  
  // Termes de calcul de l'incertitude
  double sinOcosO = sin(Ome)*cos(Ome);
  double sinwcosw = sin(w)*cos(w);       

  if(tg2iAG > tg2iBF) { // Choix de la formule de plus grand denominateur 
    
    // Calcul de i :
    incl = 2. * atan2( sqrt(std::abs((A - G) * cos(wpOme))) , sqrt(tg2iAG));
    
    // Calcul de a1 :
    a1 = sqrt( (C*C + H*H) / pow(sin(incl),2.0) );
    
    double tanis2 = tan(incl/2.0);
    
    if(tanis2 > 0) {
      double G2mA2    = G*G - A*A;  // Termes de calcul de l'incertitude
      double cosmcosp = cos(wmOme)*cos(wpOme); 
      
      double tA = 2.*G*cosmcosp + G2mA2*(sinOcosO*tAw + sinwcosw*tAO);
      double tB = G2mA2*(sinOcosO*tBw + sinwcosw*tBO);
      double tF = G2mA2*(sinOcosO*tFw + sinwcosw*tFO);
      double tG = -2.*A*cosmcosp + G2mA2*(sinOcosO*tGw + sinwcosw*tGO);
      
      sigincl = sqrt( tA*tA*covMat(0,0) + tB*tB*covMat(1,1) + tF*tF*covMat(2,2) +   
                             tG*tG*covMat(3,3) + 2.0 * ( tA*tB*covMat(0,1) +              
                             tA*tF*covMat(0,2) + tA*tG*covMat(0,3) + tB*tF*covMat(1,2) +    
                             tB*tG*covMat(1,3) + tF*tG*covMat(2,3) ) ) /                    
                             ( tanis2 * (1.0 + tanis2*tanis2) * tg2iAG*tg2iAG );
      
      // Calcul de sig_a1 :
      double tCH = (C*C + H*H) * cos(incl) / ( a1*pow(sin(incl),3) * tanis2 * (1.0 + tanis2*tanis2) * tg2iAG*tg2iAG );
      tA  = tA * tCH;
      tB  = tB * tCH;
      tF  = tF * tCH;
      tG  = tG * tCH;
      double tC  = C / ( a1 *pow(sin(incl),2) );
      double tH  = H / ( a1 *pow(sin(incl),2) );
      
      siga1 = sqrt( tA*tA*covMat(0,0) + tB*tB*covMat(1,1) + tF*tF*covMat(2,2) +   
                       tG*tG*covMat(3,3) + tC*tC*covMat(4,4) + tH*tH*covMat(5,5) 
              + 2.0 * (tA*tB*covMat(0,1)  + tA*tF*covMat(0,2)  + tB*tF*covMat(1,2)  +    
                       tA*tG*covMat(0,3)  + tB*tG*covMat(1,3)  + tF*tG*covMat(2,3)  +    
                       tA*tC*covMat(0,4)  + tB*tC*covMat(1,4)  + tF*tC*covMat(2,4)  +    
                       tG*tC*covMat(3,4)  + tA*tH*covMat(0,5) + tB*tH*covMat(1,5) +    
                       tF*tH*covMat(2,5) + tG*tH*covMat(3,5) + tC*tH*covMat(4,5) ) );
      
    } else {
      sigincl = NA_REAL;
      siga1 = NA_REAL;
    }
    
    //-------------------
    
  } else { // si tg2iBF est le plus grand denominateur
    
    // Calcul de i :
    
    incl = 2.0 * atan2( sqrt(std::abs((B + F) * sin(wpOme))) , sqrt(tg2iBF)); // 24 juin 2021
    
    // Calcul de a1 :
    a1 = sqrt( (C*C + H*H) / pow(sin(incl),2.0) );
    
    // Calcul de sig_i :
    double tanis2 = tan(incl/2.0);
    
    if(tanis2 > 0) {
      double F2mB2 = F*F - B*B; // // Termes de calcul de l'incertitude
      double sinmsinp = sin(wmOme)*sin(wpOme); 
      
      double tA = F2mB2*(-sinOcosO*tAw + sinwcosw*tAO);
      double tB =  2.0*F*sinmsinp +F2mB2*(-sinOcosO*tBw + sinwcosw*tBO);
      double tF = -2.0*B*sinmsinp +F2mB2*(-sinOcosO*tFw + sinwcosw*tFO);
      double tG = F2mB2*(-sinOcosO*tGw + sinwcosw*tGO);
      
      sigincl = sqrt( tA*tA*covMat(0,0) + tB*tB*covMat(1,1) + tF*tF*covMat(2,2) +   
        tG*tG*covMat(3,3) + 2.0 * ( tA*tB*covMat(0,1) +              
        tA*tF*covMat(0,2) + tA*tG*covMat(0,3) + tB*tF*covMat(1,2) +    
        tB*tG*covMat(1,3) + tF*tG*covMat(2,3) ) ) /                    
        ( tanis2 * (1.0 + tanis2*tanis2) * tg2iBF*tg2iBF );
      
      // Calcul de sig_a1 :
      double tCH = (C*C + H*H) * cos(incl) / ( a1*pow(sin(incl),3) * tanis2 * (1.0 + tanis2*tanis2) * tg2iBF*tg2iBF );
      tA  = tA * tCH;
      tB  = tB * tCH;
      tF  = tF * tCH;
      tG  = tG * tCH;
      double tC  = C / ( a1 *pow(sin(incl),2) );
      double tH  = H / ( a1 *pow(sin(incl),2) );
      
      siga1 = sqrt( tA*tA*covMat(0,0) + tB*tB*covMat(1,1) + tF*tF*covMat(2,2) +   
        tG*tG*covMat(3,3) + tC*tC*covMat(4,4) + tH*tH*covMat(5,5) + 2.0 * (     
            tA*tB*covMat(0,1)  + tA*tF*covMat(0,2)  + tB*tF*covMat(1,2)  +    
              tA*tG*covMat(0,3)  + tB*tG*covMat(1,3)  + tF*tG*covMat(2,3)  +    
              tA*tC*covMat(0,4)  + tB*tC*covMat(1,4)  + tF*tC*covMat(2,4)  +    
              tG*tC*covMat(3,4)  + tA*tH*covMat(0,5) + tB*tH*covMat(1,5) +    
              tF*tH*covMat(2,5) + tG*tH*covMat(3,5) + tC*tH*covMat(4,5) ) );
      
    } else {
      sigincl = NA_REAL;
      siga1 = NA_REAL;
    }
    
  }
  
  //-------------------------------------------------------------------------------
  // Calcul du demi-grand axe :
  
  double u = (A*A + B*B + F*F + G*G) / 2.0;
  double v = A*G - B*F;
    
  double racu2v2   = sqrt((u + v)*(u - v));
  a0 = sqrt( u + racu2v2 );
    
  double tA = A + (u*A - v*G)/racu2v2;
  double tB = B + (u*B + v*F)/racu2v2;
  double tF = F + (u*F + v*B)/racu2v2;
  double tG = G + (u*G - v*A)/racu2v2;
    
  siga0 = sqrt( tA*tA*covMat(0,0) + tB*tB*covMat(1,1) + tF*tF*covMat(2,2) +   
      tG*tG*covMat(3,3) + 2.0 * ( tA*tB*covMat(0,1) +              
      tA*tF*covMat(0,2) + tA*tG*covMat(0,3) + tB*tF*covMat(1,2) +    
      tB*tG*covMat(1,3) + tF*tG*covMat(2,3) ) ) / (2.0 * a0); 
  
  // 13 Oct 2022: check orbit orientation on C/H parameters,
  // circular option always leads to w=0, no info on the orientation from C/H anymore.
  if (H*cos(w)<0 && covMat(3,3)>0) {
    w+=M_PI;
    Ome+=M_PI;
  }
    
  if(w > 2*M_PI) {
    w = w - 2*M_PI; 
  } else if(w < 0) {       // Recadrage de w entre 0 et 2 pi.
    w = w + 2*M_PI;   
  }
    
  DataFrame out = DataFrame::create(_["a0"]=a0,_["inclination"]=incl*180/M_PI,_["arg_periastron"]=w*180/M_PI,_["nodeangle"]=Ome*180/M_PI,_["a1"]=a1,
                                    _["a0_error"]=siga0,_["inclination_error"]=sigincl*180/M_PI,_["arg_periastron_error"]=sigw*180/M_PI,_["nodeangle_error"]=sigOme*180/M_PI,_["a1_error"]=siga1);
  return(out);
}
